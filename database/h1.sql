DROP DATABASE IF EXISTS h1;
CREATE DATABASE IF NOT EXISTS h1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

-- 
-- Disable foreign keys
-- 
-- /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Set default database
--
USE h1;

--
-- Definition for table autores
--
CREATE TABLE IF NOT EXISTS entradas (
  id int(11) NOT NULL AUTO_INCREMENT,
  texto varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET latin1
COLLATE latin1_swedish_ci;


-- Dumping data for table entradas
--
INSERT INTO entradas VALUES
(1, 'Ejemplo de clase'),
(2, 'El Yii es facilisimo'),
(3, 'Mi mama me mima'),
(4, 'A mi la legion');
/*
-- 
-- Dumping data for table libros
--
INSERT INTO libros VALUES
(1, 'Libro1', 234, 'p1.jpg', 'Descripcion del libro'),
(2, 'Libro2', 500, 'p2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis odio nec elit tristique auctor. Nunc sapien augue, hendrerit ac eros eget, venenatis ullamcorper ligula. Vestibulum dictum dui id tellus viverra ultricies. Phasellus lectus mi, fringilla id libero nec, condimentum tristique enim. Morbi vitae orci nec nulla vestibulum tempor. In nec ligula fringilla, egestas mauris ut, mattis nisi. Donec laoreet varius enim et pretium.\r\n\r\nIn tempor sapien nec est pretium pretium. In semper quis neque et ullamcorper. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed vel lacus eu arcu aliquet malesuada. Vestibulum iaculis eget urna ut fringilla. Proin aliquet purus at leo fermentum, a finibus risus faucibus. Curabitur sed magna interdum, semper metus in, vulputate quam.\r\n\r\nSed rhoncus neque ut purus dapibus tincidunt. Pellentesque facilisis neque risus, eu viverra nulla fringilla vel. Aenean consectetur lacinia justo sit amet tincidunt. Phasellus venenatis risus lorem, vel iaculis ante efficitur sed. Etiam malesuada purus luctus, vehicula turpis eu, porttitor ligula. Fusce rhoncus, sem nec rutrum finibus, tortor ligula convallis ipsum, sed pharetra urna justo et ante. Integer non maximus neque, eu lacinia metus. Curabitur luctus odio sit amet orci pharetra, vel posuere tellus molestie. Donec a ligula quis est lobortis efficitur. Sed egestas mattis placerat. Aliquam feugiat fringilla justo vel posuere. Praesent varius quam nec eros ultrices, vitae luctus orci rhoncus. Suspendisse sit amet magna justo. Praesent eget eros ut augue dictum cursus eget non lectus. Nunc cursus metus rhoncus, varius mi eget, bibendum massa.'),
(3, 'Libro3', 150, 'p3.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lobortis odio nec elit tristique auctor. Nunc sapien augue, hendrerit ac eros eget, venenatis ullamcorper ligula. Vestibulum dictum dui id tellus viverra ultricies. Phasellus lectus mi, fringilla id libero nec, condimentum tristique enim. Morbi vitae orci nec nulla vestibulum tempor. In nec ligula fringilla, egestas mauris ut, mattis nisi. Donec laoreet varius enim et pretium.'),
(4, 'Libro4', 450, 'p4.jpg', 'tempor sapien nec est pretium pretium. In semper quis neque et ullamcorper. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed vel lacus eu arcu aliquet malesuada. Vestibulum iaculis eget urna ut fringilla. Proin aliquet purus at leo fermentum, a finibus risus faucibus. Curabitur sed magna interdum, semper metus in, vulputate quam.');

-- 
-- Enable foreign keys
-- 
-- /*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */
  */