<?php

namespace app\controllers;

use Yii;
//use yii\filters\AccessControl;
use yii\web\Controller;//Class yii\web\Controller(consultar API)
//use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Entradas;// Se necesita el modelo en el controlador
//use app\models\ContactForm;
use yii\data\ActiveDataProvider;
class SiteController extends Controller
{
  
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index'); // render carga las vistas 
    }


    public function actionListar()
    {
        $s=Entradas::find() -> asArray()->all();  
        /*
         foreach ($datos as $registro){
            var_dump($registro);
        }
        */
        //public string render ( $view, $params = [], $context = null )
        return $this->render('listar', [ 
            "datos"=>$s,
        ]);
    }
            

    public function actionListar1()
    {
        $s=Entradas::find()->all();
        return $this->render('listarTodos',
                [
                    "datos"=>$s,
                ]);
    }
    public function actionListar2()
    {
        $salida=Entradas::find()->select(['texto'])->asArray()->all();
        return $this->render('listarTodos', ["datos"=>$salida]);
    }
    public function actionListar3()
    {
        $salida=Entradas::find()->select(['texto'])->all();
        return $this->render('listarTodos', ["datos"=>$salida]);
    }
    public function actionListar4()
    {
        $salida=new Entradas();
        return $this->render('listarTodos', ["datos"=>$salida->find()->all()]);
    }
    public function actionListar5()
    {
        $salida=new Entradas();
        return $this->render('listarTodos', ["datos"=>$salida->findOne(1)]);
    }
    public function actionListar6()
    {
        return $this->render('listarTodos', [
            "datos"=>Yii::$app->db->createCommand("Select * from entradas")->queryAll(),
            ]);
    }
    
    public function  actionMostrar()
    {
        $dataProvider = new ActiveDataProvider([
            'query'=>Entradas::find(),
        ]);
        return $this->render('mostrar', [
            'dataProvider'=>$dataProvider,
        ]);
    }  
    
    public function actionMostraruno()
    { 
       return $this->render('mostrarUno', [
           'model'=>Entradas::findOne(1),
       ]);
    }       
}    
